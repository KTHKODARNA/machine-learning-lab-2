from cvxopt.solvers import qp
from cvxopt.base import matrix

import numpy, pylab, random, math

#GENERATE TESTDATA
classA = [(random.normalvariate(-1.5, 1),
		 random.normalvariate(0.5, 1), 1.0)
		for i in range(10)] + \
		[(random.normalvariate(1.5, 1),
			random.normalvariate(0.5, 1),
			1.0)
		for i in range(10)]

classB = [(random.normalvariate(2.0, 0.5), 
		random.normalvariate(-2.5, 0.5), 
		-1.0)
		for i in range(10)]

classB += [(random.normalvariate(-2.0, 0.7), 
		random.normalvariate(3, 0.7), 
		-1.0)
		for i in range(10)]

data = classA + classB
random.shuffle(data)
#GENERATE TESTDATA

# #GENERATE TESTDATA
# classA = [(random.normalvariate(2.0, 0.5),
# 		 random.normalvariate(-2.0, 0.5), 1.0)
# 		for i in range(20)] + \
# 		[(random.normalvariate(-2.0, 0.5),
# 			random.normalvariate(2.0, 1),
# 			1.0)
# 		for i in range(20)]

# classB = [(random.normalvariate(0.0, 0.5), 
# 		random.normalvariate(0.0, 0.5), 
# 		-1.0)
# 		for i in range(20)]

# data = classA + classB
# random.shuffle(data)
# #GENERATE TESTDATA

def linear_kernel(x1, x2):
	ret = numpy.dot(x1, x2)
	ret = ret + 1
	return ret;

def radial_kernel(x1, x2):
	return numpy.exp(-numpy.linalg.norm(numpy.array(x1)-numpy.array(x2),2)**2/sigma**2)

def polynomial_kernel(x1, x2):
	ret = numpy.dot(x1, x2)
	ret = ret + 1
	ret = ret**p
	return ret;

def build_q_Vector(n):
	#q = [float(-1)] * n
	q=numpy.ones(n)*-1	
	return q;

def build_h_Vector(n):
	#h = [float(0)] * n;
	h = numpy.zeros(n)
	return h;

def build_G_Matrix(n):
	g = numpy.zeros((n,n), float)
	numpy.fill_diagonal(g, float(-1));
	return g

def build_P_Matrix(X):
	n = len(X)
	#P = [[0 for x in range(n)] for y in range(n)]
	P = numpy.zeros((n, n))
	for i in range(n):
		for j in range(n):
			x1 = [data[i][0],data[i][1]]
			x2 = [data[j][0],data[j][1]]
			ti = data[i][2]
			tj = data[j][2]
			#P[i,j] = linear_kernel(x1, x2) * ti * tj #linear kernel
			P[i,j] = radial_kernel(x1, x2) * ti * tj #radial kernel
			#P[i,j] = polynomial_kernel(x1, x2) * ti * tj #poly kernel

	return P

def plotData():
	pylab.hold(True)
	pylab.plot([p[0] for p in classA],
				[p[1] for p in classA],
				'bo')
	pylab.plot([p[0] for p in classB],
				[p[1] for p in classB],
				'ro')
	#pylab.plot([p[1][0] for p in alpha], [p[1][1] for p in alpha], 'yo')
	pylab.show()

def pickAlphas(alpha):
	a = []
	for i in range(len(alpha)):
		if alpha[i]>10e-5:
			a.append([alpha[i], data[i]])
	return a

def indicator(xstar):
	res = 0
	for i in range(len(alpha)):
		#res += alpha[i][0]*alpha[i][1][2]*linear_kernel(xstar,[alpha[i][1][0], alpha[i][1][1]]) #linear kernel
		res += alpha[i][0]*alpha[i][1][2]*radial_kernel(xstar,[alpha[i][1][0], alpha[i][1][1]]) #radial kernel
		#res += alpha[i][0]*alpha[i][1][2]*polynomial_kernel(xstar,[alpha[i][1][0], alpha[i][1][1]]) #polynomial kernel

	return res

p = 2 # exponent value for polynomial kernel
sigma = 3 # for radial kernel
n = len(data)
q = build_q_Vector(n)
h = build_h_Vector(n)
G = build_G_Matrix(n)
P = build_P_Matrix(data)
r = qp(matrix(P), matrix(q), matrix(G), matrix(h))
alpha = pickAlphas(list(r['x']))
print alpha
xrange = numpy.arange(-4,4,0.05)
yrange = numpy.arange(-4,4,0.05)

grid = matrix([[indicator([x,y]) for y in yrange] for x in xrange])

pylab.contour(xrange, yrange, grid, (-1.0, 0.0, 1.0), colors=('red','black','blue'), linewidths = (1,3,1))

plotData()








